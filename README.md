# Sommaire
- [Sommaire](#sommaire)
  - [Introduction](#introduction)
  - [Prerequis](#prerequis)
  - [Les bases d'ansible](#les-bases-dansible)
    - [Qu'est ce qu'Ansible ?](#quest-ce-quansible)
    - [L'idempotence](#lidempotence)
    - [La documentation Ansible](#la-documentation-ansible)
    - [Explication de l'arborescence ansible](#explication-de-larborescence-ansible)
    - [Langage ansible : yaml, python](#langage-ansible--yaml-python)
  - [Utilisation d'ansible](#utilisation-dansible)
    - [Les types de connexions](#les-types-de-connexions)
    - [Configuration ssh](#configuration-ssh)
    - [Détail du fichier de configuration](#d%c3%a9tail-du-fichier-de-configuration)
    - [Les commandes ad-hoc](#les-commandes-ad-hoc)
  - [Playbook](#playbook)
    - [Contexte](#contexte)
    - [Création du playbook](#cr%c3%a9ation-du-playbook)
    - [Déploiement d'un playbook](#d%c3%a9ploiement-dun-playbook)
    - [Modifier la page d'accueil du site](#modifier-la-page-daccueil-du-site)
  - [Inventaire](#inventaire)
    - [Création des inventaires](#cr%c3%a9ation-des-inventaires)
    - [Déploiement à partir d'un inventaire](#d%c3%a9ploiement-%c3%a0-partir-dun-inventaire)
    - [Ajout du load balancer](#ajout-du-load-balancer)
    - [Handlers](#handlers)
  - [Role](#role)
    - [Ansible-galaxy](#ansible-galaxy)
    - [Structure](#structure)
    - [Creation du role apache (serveur web)](#creation-du-role-apache-serveur-web)
    - [Creation du role de haproxy (load balancer)](#creation-du-role-de-haproxy-load-balancer)
    - [Creation du playbook de deploiement des roles](#creation-du-playbook-de-deploiement-des-roles)
    - [Déploiement des roles](#d%c3%a9ploiement-des-roles)
    - [Verification](#verification)
  - [Molecule](#molecule)
    - [Qu'est ce que Molecule ?](#quest-ce-que-molecule)
    - [Pourquoi l'utiliser ?](#pourquoi-lutiliser)

## Introduction

L'objectif de ce hands-on ansible débutant est de présenter l'outil ansible, qu'est ce que c'est et comment l'utiliser. Il se découpe en deux grandes parties : une première plus théorique possera les bases d'ansible et une deuxième partie de mise en pratique permettra de valider et d'étoffer certaines notions.   

## Prerequis

Des prérequis sont nécessaire pour suivre correctement ce hands-on :

  - avoir une bonne expérience dans l'administration système linux
  - maitriser un editeur de code 
  - maitriser la connexion ssh par clé
  
## Les bases d'ansible

### Qu'est ce qu'Ansible ?

Ansible est un outil permettant d'automatiser la configuration de machines aussi bien linux que windows. Pour faire simple, ansible se base sur des fichiers yaml contenant la listes des actions à réaliser. Ces fichiers sont variabilisés pour pouvoir les adapter en fonction de l'environnement sans avoir à modifier le code des actions.

Contrairement à certains de ses concurrents, Ansible est agent-less ce qui signifie qu'il n'y a rien à installer sur les machines cibles. Une connexion SSH (pour linux) ou WinRM (pour windows) est le seul prérequis pour déployer les configurations.

### L'idempotence

Ansible ne se charge pas d'exécuter bêtement les actions les unes à la suite des autres, il va chercher à atteindre un état cible.
C'est à dire qu'une action s'exécutera uniquement si l'état cible de la machine n'est pas celui attendu. Il s'agit de l'idempotence. Les actions peuvent être exécutés autant de fois que l'on veut, le résultat final sera toujours le même.

### La documentation Ansible

Documentation : 

La [Documenation ansible](https://docs.ansible.com/ansible/latest/index.html) est de très bonne qualité, il ne faut pas hésiter à en abuser.

Voici par exemple la documentation des modules ansible disponible : https://docs.ansible.com/ansible/latest/modules/list_of_all_modules.html

### Explication de l'arborescence ansible

Documentation : https://docs.ansible.com/ansible/latest/user_guide/playbooks_best_practices.html

Ansible laisse le choix entre plusieurs structure pour organiser notre code. Il n'est pas nécessaire de tout comprendre des le début. Nous utiliserons la structure alternative au cours de ce hands-on :

```
inventories/
   dev/                   # environnement de developpement
      hosts               # fichier contenant la liste des machines d'un environnement
      group_vars/
         group1.yml       # variables spécifiques à un groupe de machine
         group2.yml
      host_vars/
         hostname1.yml    # variable spécifiques à une machine de l'inventaire
         hostname2.yml

   prod/                  # environnement de production
      hosts               
      group_vars/
         group1.yml       
         group2.yml
      host_vars/
         stagehost1.yml   
         stagehost2.yml

files/                    # contient les fichiers plat à déployer
templates/                # contient les fichiers variabilisé à déployer

apache.yml                # playbook permetant de deployer un serveur apache (à partir du role apache)
haproxy.yml               # playbook permettant de deployer un serveur haproxy (à partir du role haproxy)
infra.yml                 # playbook faisant appel aux deux précedents pour déployer l'infra complète.

roles/                    # repertoire contenant l'ensemble des roles ansible
    apache/               # role pour déployer un serveur apache 
    haproxy/              # role pour déployer un serveur haporxy 
```

Nous détaillerons au cours de ce hands-on étape par étape tous les éléments de cette strucure à l'exception des groups_vars et host_vars qui seront abordés dans un prochain hands_on.

### Langage ansible : yaml, python

Ansible est développé en python et sa structure permet de facilement rajouter de nouvelles fonctionnalités pour améliorer l'outil et augmenter ses fonctionnalités (cf module).

La configuration et l'utilisation d'ansible se fait via des fichiers yaml. La syntaxe yaml à l'avantage d'être facilement lisible en plus de structure et donner de la cohérence dans le code ansible.

Voici un exemple de fichier ansible contenant une liste de tâches :

```yaml
- name: Installation de la derniere version de httpd
  yum:           
      name: httpd
      state: present

- name: Installation de la derniere version de vim
  yum:           
      name: vim
      state: present

- name: Desintallation de la nginx
  yum:           
      name: nginx
      state: absent
```

Une tâche représentant une opération qui sera exécuté sur la machine cible. Dans l'xemple ci-dessus, on comprend très rapidement le but de chaque tâche : installer httpd et vim, supprimer nginx.

## Utilisation d'ansible

### Les types de connexions

Documentation : https://docs.ansible.com/ansible/latest/plugins/connection.html

Ansible se connecte sur les machines cibles afin d'exécuter des tâches. Par défaut la connexion utilisé est le ssh mais il en existe beaucoup d'autres. 

Parmi les plus courantes on peut retouver local pour exécuter des tâches directement sur l'host, winRM pour se connecter à une machine windows ...

### Configuration ssh

Le configuration ssh sans mot de passe permet d'exécuter ansible sans avoir à renseigner le mot de passe.

Les étapes consistent à générer une paire de clé ssh sur le serveur ansible puis d'autoriser la clé publique sur toutes les machines cibles où l'on souhaite déployer.

```
[<user>@localhost ]$ ssh-keygen
[<user>@localhost ]$ cat ~/.ssh/id_rsa.pub > ~/.ssh/authorized_keys
```

### Détail du fichier de configuration

https://docs.ansible.com/ansible/latest/installation_guide/intro_configuration.html#configuration-file

La configuration ansible peut etre surchargé via le fichier ansible.cfg. Il est composé de plusieurs sections afin de modifier l'utilisateur ssh par défaut, surcharger les paramètres ssh ... :

```
[defaults]
# Utilisateur utilise pour la connexion ssh
remote_user = ansible

[ssh_connection]
# Surcharge des parametres ssh
ssh_args = -C -o StrictHostKeyChecking=no -o ControlMaster=auto -o ControlPersist=60s
```

Le fichier ansible.cfg se trouve généralement à la racine du playbook que l'on souhaite exécuter afin qu'il soit pris en compte lors de l'éxécution.

### Les commandes ad-hoc

Documentation : https://docs.ansible.com/ansible/latest/user_guide/intro_adhoc.html

Une commande ad-hoc permet d'exécuter une tâche sur une ou plusieurs machines. Les commandes ad-hoc sont faciles et rapides à utiliser mais pas réutilisable. C'est une bon moyen de comprendre le fonctionnement d'ansible.

Voici la syntaxe d'une commande ad-hoc :

```
ansible [pattern] -i '<ip>,' -m [module] -a "[module options]"
```

Le pattern correspond à un groupe de machine. Nous n'avons pas encore de groupe donc nous utiliserons le pattern `all`. 
Un module correspond à une tâche, une opération à exécuter. Par exemple l'installation d'un paquet, la création d'un utilisateur ...

Voici des exemples d'usage :

```
# execution de la commande echo via le module shell d'ansible
ansible all -i '<ip_cible_1>,' -m shell -a 'echo $TERM'

# verification si le service ssh est demarre via le module service d'ansible
ansible all -i '<ip_cible_1>,<ip_cible_2>' -m service -a "name=sshd state=started"
```

## Playbook

Ansible exécute de manière séquentielle une liste de tâches. Les tâches sont renseignées dans un playbook au format yaml.

La structure d'une tâche est la suivante (l'indentation est importante) :
```yaml
- name: Installation de la derniere version de httpd # Description de la tache
  become: true                                       # activer l'elevation de privilege (default sudo)
  # cf doc pour avoir la liste exhaustive des parametres d'une tache
  yum:                                               # nom du module pour réaliser une action            
      name: httpd
      state: present
      # cf doc pour avoir la liste exhaustive des parametres du module
      # https://docs.ansible.com/ansible/latest/modules/yum_module.html
```

Les taches appartiennent à un "bloc" de plus haut niveau pour indiquer la cible où elles seront exécutés :

```yaml
- name: "Installation et demarrage du serveur web"
  hosts: all # L'ensemble des taches ci-dessous seront exécutés sur toutes les machines
             # Nous verrons par la suite la notion de groupe pour séparer les machines
  become: true
  tasks:
    # Liste des taches a executer
    - name: Installation de la derniere version de httpd
      yum:
        name: httpd
        state: present

    - name: Demarrage du service httpd
      systemd:
        name: httpd
        state: started
```

Comme vous avez dû le remarquer, le mot clé become peut être utilisé au niveau d'une tache ou a l'ensemble des taches.

### Contexte

L'objectif est de créer un playbook pour déployer un serveur web et modifier sa page d'accueil :
- environnement : centos-7
- package : httpd

### Création du playbook

```
mkdir ~/workspace
touch ~/workspace/install.yml

vim ~/workspace/install.yml
```
```yaml
- name: "Installation et demarrage du serveur web"
  hosts: all
  become: true
  tasks:
    - name: Installation de la derniere version de httpd
      yum:
        name: httpd
        state: present

    - name: Demarrage du service httpd
      systemd:
        name: httpd
        state: started
```

Le playbook ci-dessus permet deux choses :
- installer le paquet httpd dans sa derniere version à l'aide du module yum
- démarrer le service httpd à l'aide du module systemd

### Déploiement d'un playbook

Maintenant que nous avons notre playbook nous pouvons le déployer :

```bash
# Execution du playbook sur une machine cible spécifique 
ansible-playbook install.yml -i <ip_machine_cible>, -u demo

PLAY [Installation et demarrage du serveur web] *************************************************

TASK [Gathering Facts] *************************************************************************************************
ok: [172.17.0.2]

TASK [Installation de la derniere version de httpd] *************************************************************************************************
changed: [172.17.0.2]

TASK [Demarrage du service httpd] *************************************************************************************************
changed: [172.17.0.2]

PLAY RECAP *************************************************************************************************
172.17.0.2                 : ok=4    changed=2    unreachable=0    failed=0    skipped=0    rescued=0    ignored=0  
```

Vu qu'il s'agit de notre premier déploiement les taches d'installation et de démarrage sont dans l'état changed. 

Ansible étant idempotent, si on relance le deploiement, les taches seront dans l'état `ok` et rien ne sera exécuté (l'état cible étant déjà atteint).

Pour vérifier que le serveur web est bien démarré :

```
curl <ip>
```

### Modifier la page d'accueil du site

Nous souhaitons désormais modifier la page d'accueil de notre site. Pour se faire nous allons utiliser le module copy d'ansible pour déposer un fichier plat sur la machine cible.

Dans un premier temps nous allons créer le repertoire files qui contiendra notre page d'accueil custom :

```bash
mkdir ~/files
touch ~/files/index.html

vim ~/files/index.html
# exemple de contenu
#------------------------#
# DEMO
#------------------------#
```

Nous allons maintenant rajouter une task dans notre playbook pour deposer ce fichier (à l'aide du module copy) :

/!\ L'execution d'un playbook ansible est séquentiel, l'ordre des tâches peut avoir son importance. Dans notre cas, il vaut mieux déposer le fichier index.html après avoir installé notre seveur web.

vim install.yml
```yaml
...
    - name: Custom index.html
      become: true
      copy:
        src: index.html
        dest: /var/www/html/
...
```

En relancant un deploiement la nouvelle tache apparait dans les logs : 

```
TASK [Custom index.html] *********************************************************************************************************************************************************************************************************************************************
changed: [172.17.0.2]
```

Exécuter la commande `curl <ip>` pour vérifier le nouveau contenu du site.

## Inventaire

Un inventaire ansible référence l'ensemble des machines d'un environnement. Elles sont rangées par groupe et peuvent contenir des informations supplémentaires : ip, login de connexion ssh, type de connexion ...

### Création des inventaires

Documentation : https://docs.ansible.com/ansible/latest/user_guide/intro_inventory.html

```
[<user>@localhost ]$ mkdir -p workspace/{inventory/dev,inventory/rec,/inventory/prod}
[<user>@localhost ]$ cd workspace
[<user>@localhost ]$ touch inventory/dev/hosts inventory/rec/hosts inventory/prod/hosts

[<user>@localhost ]$ tree
├── inventory
│   └── dev
│       └── hosts
│   └── qualif
│       └── hosts
│   └── prod
│       └── hosts
└── install.yml
```

```
vim inventory/dev/hosts

[web]
web01 ansible_host=172.17.0.2 ansible_user=demo
web02 ansible_host=172.17.0.3 ansible_user=demo
```

Par défaut la connexion se fera en ssh mais il est possible de la modifier via le paramètre ansible_connection (ansible_connection=ssh).

### Déploiement à partir d'un inventaire

Execution du playbook a partir d'un inventaire :

```
ansible-playbook install.yml -i inventory/<inventory_name>
# exemple : ansible-playbook install.yml -i inventory/dev
```

On peut limiter le depoiement à un groupe ou une machine :

```
# Deploiement sur un groupe
ansible-playbook install.yml -i inventory/dev --limit web

# Deploiement sur une machine 
ansible-playbook install.yml -i inventory/dev --limit web01

# Deploiement sur plusieurs machines
ansible-playbook install.yml -i inventory/dev --limit web01,web02
```

### Ajout du load balancer

Modifier l'inventory pour ajouter le serveur de load balancing :

```
[load_balancer]
load-balancer01 ansible_host=172.17.0.4 ansible_user=demo
```

Modifier le playbook pour que l'installation des serveurs webs soient à destination du groupe web et rajouter l'installation du load balancer :

```yaml
- name: "Installation et demarrage du serveur web"
  hosts: web
  become: true

  ...

- name: "Installation et demarrage du serveur haproxy"
  hosts: load_balancer
  become: true
  tasks:
    - name: Installation de la derniere version de haproxy
      yum:
        name: haproxy
        state: present

    - name: Custom configuration
      become: true
      copy:
        src: haproxy.cfg
        dest: /etc/haproxy/haproxy.cfg

    - name: Demarrage du service haproxy
      systemd:
        name: haproxy
        state: started
```

Voici un exemple de fichier de configuration de haproxy :

```
/etc/haproxy/haproxy.cfg

global
  log 127.0.0.1 local0
  stats socket /run/haproxy.sock level admin
  stats timeout 30s
  pidfile /run/haproxy.pid
  daemon
  user haproxy
  group haproxy

defaults
  log global
  mode tcp
  retries 3
  timeout connect 1m
  timeout client 3m
  timeout server 3m
  option log-health-checks



frontend haproxy_frontend_http
  bind 0.0.0.0:80
  default_backend haproxy_backend_http
  option tcplog


backend haproxy_backend_http
  mode tcp
  option httpchk GET /

  server srv_backend1 <ip_srv_web1>:80 id 1
  server srv_backend2 <ip_srv_web1>:80 id 2
```

Il n'est pas possible avec un curl sur le serveur haproxy de savoir lequel des deux serveurs web nous répond car l'index est identique. Nous allons faire une petite modification pour que cette page soit différente en fonction du serveur.

Pour cela nous allons utiliser les templates. Ce sont des fichiers qui peuvent etre variabilisé. Nous allons créer un répertoire templates et y déposer notre nouvel index.html :

```
#------------------------#
# DEMO {{ inventory_hostname }}
#------------------------#
```

Les `{{ }}` indique qu'il s'agit d'une variable. Elle sera remplacé à l'exécution du playbook. La variable `inventory_hostname` est une variable interne à ansible contenant le hostname de la machine cible. Ainsi, il sera différent pour chaque machine et dans notre cas pour chaque serveur web.

Il ne reste plus qu'à remplacer dans notre playbook le nom du module copy par template et relancer un deploiement.

```yaml
template:
  src: index.html
  dest: /var/www/html/
```

L'execution de la commande curl à destination de notre serveur haproxy devrait cette fois-ci retourner une fois web01 puis web02 :

```
[root@localhost hands-on]# curl <ip_haproxy>
#------------------------#
# DEMO web01
#------------------------#

[root@localhost hands-on]# curl <ip_haproxy>
#------------------------#
# DEMO web02
#------------------------#
```

### Handlers

Contrairement au fichier index.html qui ne necessite pas de redémarrage du service pour etre pris en compte, la configuration de haproxy en a besoin.

Avec le playbook actuel a aucun moment on lui demande de redémarrer. Pour lancer un redémarrage apres une tache precise il faut utiliser les handlers. Ce sont des taches exécutées uniquement lors d'un `change` grace au mot clé notify.

Les handlers se trouvent dans le répertoire handlers :

```
mkdir ~/workspace/handlers/main.yml
```

La syntaxe est identique à celle des taches d'un playbook si ce n'est qu'il faut rajouter la ligne listen :

```
vim ~/workspace/handlers/main.yml
```

```yaml
- name: Restart du service haproxy
  systemd:
    name: 'haproxy'
    state: restarted
    daemon_reload: true
    enabled: true
  listen: haproxy_restart
```

La ligne `listen` permet de faire le lien entre les taches de notre playbook et notre listener. 

Dans le playbook install.yml rajouter la ligne notify :

```yaml
- name: Custom configuration
  become: true
  copy:
    src: haproxy.cfg
    dest: /etc/haproxy/haproxy.cfg
  notify: haproxy_restart
```

Des qu'il y aura un changement dans le template haproxy.cfg un notify sera envoyé et exécutera la tache présente dans les handlers correspondant au listener haproxy_restart. Celui-ci lancera un redémarrage du service haproxy.

## Role

Un rôle Ansible est un ensemble de tâches qui s’assurent de la présence/absence d’une fonctionnalité spécifique. Chaque rôle doit être capable de fonctionner en autonomie.

L'objectif est de factoriser le code ansible afin de pouvoir le réutiliser au besoin. 

### Ansible-galaxy

Documenation : https://docs.ansible.com/ansible/latest/galaxy/user_guide.html

La commande ansible-galaxy permet d'initialiser un nouveau role avec une structure par défaut.

Elle permet également de récupérer des roles dans une version précise à partir d'une multitude de source (git, serveur web, ...). Cette commande nécessite un fichier de requirement qui contiendra la liste des roles que nous souhaitons utiliser : 

```yaml
# from GitHub, overriding the name and specifying a specific tag
- name: apache
  src: https://url.git.example/apache
  version: master

# from a webserver, where the role is packaged in a tar.gz
- name: apache
  src: https://url.webserver.example/files/apache.tar.gz
```

Il suffit ensuite de lancer la commande ansible-galaxy pour récupérer les roles renseigné dans le fichier de requirement :

```
ansible-galaxy role install -r requirements.yml
```

Les roles seront télécharger dans le répertoire des roles et pourront etre utilisé dans un playbook.

### Structure

Création de l'arborescence d'un role :

```
[<user>@localhost ]$ ansible-galaxy init roles/apache

[<user>@localhost ]$ tree roles/
apache/
├── defaults
│   └── main.yml
├── files
├── handlers
│   └── main.yml
├── meta
│   └── main.yml
├── README.md
├── tasks
│   └── main.yml
├── templates
├── tests
│   ├── inventory
│   └── test.yml
└── vars
    └── main.yml
```

La structure d'un role est similaire à ce que nous avons vu depuis le début. Les répertoires vars et defaults contiennent les variables du roles. Nous verrons à quoi ils servent dans un prochain hands-on.

Le répertoire meta contient les métadonnées de ce rôle telles que son auteur, ses dépendances, sur quel type d'os il est compatible ...

### Creation du role apache (serveur web)

Maintenant que nous avons la struture de notre role apache, nous allons déplacer notre existant dans celui-ci. 

Le fichier tasks/main.yml contiendra les taches de déploiement du serveur web :

```yaml
- name: Installation de la derniere version de httpd
  yum:
    name: httpd
    state: present

- name: Custom index.html
  become: true
  template:
    src: index.html
    dest: /var/www/html/

- name: Demarrage du service httpd
  systemd:
    name: httpd
    state: started
```

Contrairement à notre playbook, ce fichier ne contient pas le bloc :

```yaml
- name: "Installation et demarrage du serveur web"
  hosts: web
  become: true
  tasks:
```

Celui-ci est réservé au playbook car il est spécifique à une configuration (ici au groupe web) contrairement au role qui se doit d'etre le plus générique possible.

Maintenant nous allons déplacer le template index.html et notre role apache sera terminé :

```
mv templates/index.html roles/apache/templates/  
```

### Creation du role de haproxy (load balancer)

Le principe pour la création du role haproxy est identique :
 - générer la structure du role haproxy avec ansible-galaxy
 - déplacer les taches dans le fichier roles/haproxy/tasks/main.yml
 - déplacer le fichier de configuration dans roles/haproxy/files
 - déplacer le fichier de handlers dans roles/haproxy/handlers

### Creation du playbook de deploiement des roles

Nous allons modifier notre playbook pour que celui-ci utilise les roles apache et haproxy. Au lieu d'appeler des taches, ce sera des roles :

Ouvrir le playbook `install.yml` :

```yaml
- name: "Installation et demarrage du serveur web"
  hosts: web
  become: true
  roles:
    - apache

- name: "Installation et demarrage du serveur haproxy"
  hosts: load_balancer
  become: true
  roles:
    - haproxy
```

Nous avons simplement remplacé le bloc tasks par roles.

### Déploiement des roles

L'exécution est identique aux précédents déploiements : `ansible-playbook install.yml -i inventory/dev`

Nous avons seulement déporter les taches dans des roles spécifiques pour rendre notre code plus flexible et réutilisable. 

### Verification

Pour vérifier que tout fonctionne toujours, vous pouvez exécuter la commande curl à destination du load balancer et vérifier que les deux serveurs webs repondent bien l'un apres l'autre :

`curl <ip_haporxy>`

## Molecule

### Qu'est ce que Molecule ?

Molecule est un outil développé en python dont le but est de tester des rôles ansible. On peut élargir son utilisation pour tester tout type de code ansible (filter plugin, module, playbook ...). Molecule apporte également un confort et une rigueur dans le écriture de code ansible.

Il permet d’initialiser un environnement, déployer un rôle, exécuter des tests sur la machine cible et enfin supprimer l’environnement.
Cette suite d’action est appelé scenario et peut contenir des étapes supplémentaires comme par exemple le test de l’idempotence du rôle. 

Voici les principales fonctionnalités de molecule :

- Créer un environnement de test from scratch : docker, vagrant, openstack, aws ...
- Tester le deploiement d'un rôle sur différents OS : redhat, centos, ubuntu ...
- Analyser la syntaxe yaml et ansible
- Tester l'idempotence et le checkmode d’un rôle.
- Exécuter des tests post déploiement à l’aide d’un framework de test : testinfra, InSpec, goss …

### Pourquoi l'utiliser ?

Isolation du développement d’un rôle :

- Test des rôles en local avec docker (centos 6.6 et 7.3)
- Evite de solliciter un environnement du projet
- Isole le développement et évite tout impact sur les environnements projets

Robustesse du rôle :

- Limite les régressions sur un role (en augmentant le nombre de tests)
- Test from scratch d’un déploiement
- Test unitaire du role
- Améliore la qualité du développement

